***Settings***
Library     SeleniumLibrary
Resource   recursos.robot

***Test Cases***
W001 ir al blog de Winston Castillo
    Open Homepage
    Title Should Be     Hola Mundo!
    Set Focus To Element    xpath=/html/body/div[1]/div/div[2]/a[1]
    Click Link      xpath=/html/body/div[1]/div/div[2]/a[1]
    Wait Until Element is Visible   xpath=//*[@id="page-header"]/div[1]/div/div/div/a
    Title Should Be     Winston Castillo – Un sitio para comunicarse
    Close Browser

W002 abrir ventana modal
    Open Homepage
    Title Should Be     Hola Mundo!
    Set Focus To Element    xpath=/html/body/div[1]/div/div[2]/a[2]
    Click Link      xpath=/html/body/div[1]/div/div[2]/a[2]
    Title Should Be     Hola Mundo!
    Wait Until Element is Visible   xpath=//*[@id="exampleModal"]/div/div/div[1]
    Close Browser