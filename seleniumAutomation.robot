***Settings***
Library     String
Library     SeleniumLibrary

***Variables***
${browser}  chrome
${landing}  automationpractice.com/index.php
${scheme}   http
${testURL}  ${scheme}://${landing}

***Keywords***
Open Homepage
    Open browser    ${testURL}      ${browser}

***Test Cases***
C001 Hacer click en contenedores
    Open Homepage
    Set Global Variable    @{nombresDeContenedores}    //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a     //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a    //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a   //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    :FOR    ${nombreDeContenedor}   IN      @{nombresDeContenedores}
    \   RUN Keyword If      '${nombreDeContenedor}' == '//*[@id="homefeatured"]/li[7]/div/div[2]/h5/a'  Exit FOR Loop
    \   Click Element      Xpath=${nombreDeContenedor}
    \   Wait Until Element is Visible   xpath=//*[@id="bigpic"]
    \   Click Element   xpath=//*[@id="header_logo"]/a/img
    Close Browser

C002 Caso de prueba Nuevo
    Open Homepage
    

